import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import * as metaData from '../../assets/mock/metadata/metadata.json';
import * as usersData from '../../assets/mock/users/users.json';
import * as portfoliosData from '../../assets/mock/porfolios/portfolios.json';
@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return of(null).pipe(mergeMap(() => {

            // authenticate
            if (request.url.endsWith('/login') && request.method === 'POST') {
                const users = (<any>usersData).users;

                const filteredUsers = users.filter(user => {
                    return user.username === request.body.username && user.password === request.body.password;
                });

                if (filteredUsers.length) {

                    const user = filteredUsers[0];
                    const body = {
                        id: user.id
                    };

                    return of(new HttpResponse({ status: 200, body: body }));
                } else {

                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }

            }

            if (request.url.endsWith('/my_portfolios/metadata') && request.method === 'GET') {
                const { default: metadata } = (<any>metaData);
                const { portfolio_attribute_elements: columnsKeys } = metadata.sections.section_portfolio_attributes;
                const { portfolio_attribute_elements: columns } = metadata;
                const tableDefinitions = [];
                columnsKeys.forEach(key => {
                    tableDefinitions.push({ definition: key, column: columns[key] });
                });
                if (tableDefinitions) {
                    return of(new HttpResponse({ status: 200, body: tableDefinitions }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }

            if (request.url.endsWith('/my_portfolios/data') && request.method === 'GET') {
                const { default: { portfolios } } = (<any>portfoliosData);

                if (portfolios) {
                    return of(new HttpResponse({ status: 200, body: portfolios.items }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }

            return next.handle(request);

        }))

            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }
}

export let fakeBackendProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
