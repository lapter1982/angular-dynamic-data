import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() logoutFromNav = new EventEmitter();
  constructor() { }


  ngOnInit() {
  }

  onLogout() {
    this.logoutFromNav.emit(null);
  }
}
