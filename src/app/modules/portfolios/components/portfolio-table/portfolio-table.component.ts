import { Component, OnInit, Input } from '@angular/core';
import { TableColumnDefinition } from 'src/app/shared/models/table-column-definition.model';

@Component({
  selector: 'app-portfolio-table',
  templateUrl: './portfolio-table.component.html',
  styleUrls: ['./portfolio-table.component.scss']
})
export class PortfolioTableComponent implements OnInit {
  @Input() columns: TableColumnDefinition[];
  @Input() dataSource: any[];

  displayedColumns: any[];

  constructor() { }

  ngOnInit() {
    this.displayedColumns = this.columns.map(c => c.columnDef);
  }


}



