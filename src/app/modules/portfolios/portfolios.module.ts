import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './pages/portfolio.component';
import { PortfolioTableComponent } from './components/portfolio-table/portfolio-table.component';
import { SharedModule } from 'src/app/shared';

@NgModule({
  declarations: [PortfolioComponent, PortfolioTableComponent],
  imports: [
    CommonModule,
    PortfolioRoutingModule,
    SharedModule
  ],
})
export class PortfoliosModule { }
