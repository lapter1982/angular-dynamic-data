import { Component, OnInit } from '@angular/core';
import { MetaDataService } from '../../../services/meta-data.service';
import { map } from 'rxjs/operators';
import { TableColumnDefinition } from 'src/app/shared/models/table-column-definition.model';
import { PortfolioService } from '../services/portfolio.service';
import {forkJoin } from 'rxjs';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  metaData: any;
  tableDefinitions: TableColumnDefinition[] = null;
  portfolioData: any[] = null;
  constructor(private metaDataService: MetaDataService, private portfolioService: PortfolioService) {
  }

  ngOnInit() {
    const metadata = this.metaDataService.getPortfolioMetaData();
    const protfoliosData = this.portfolioService.getPortfolios();
    forkJoin([metadata, protfoliosData]).subscribe((data) => {
      this.tableDefinitions = data[0];
      this.portfolioData = data[1];
    });


  }

}
