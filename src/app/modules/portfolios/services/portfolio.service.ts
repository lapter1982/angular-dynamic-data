import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(private apiservice: ApiService) { }

  getPortfolios(): Observable<any[]> {
    return this.apiservice.get('/my_portfolios/data')
      .pipe(map(data =>
        data.map(v => {
          // tslint:disable-next-line:prefer-const
          let jsonData = {};
          Object.keys(v).forEach((key) => {
            const columnName = key;
            jsonData[columnName] = v[key].value;
          });
          return jsonData;
        })));
  }
}
