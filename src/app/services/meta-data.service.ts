import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TableColumnDefinition } from '../shared/models/table-column-definition.model';
import { ApiService } from '../http/api.service';
import { map } from 'rxjs/operators';
import { FormatData } from '../shared';
@Injectable({
  providedIn: 'root'
})
export class MetaDataService {

  constructor(private apiservice: ApiService) { }
  getPortfolioMetaData(): Observable<Partial<TableColumnDefinition[]>> {
    return this.apiservice.get('/my_portfolios/metadata').pipe(map((data: any[]) =>
      data.map((v) => <TableColumnDefinition>{
        columnDef: v.definition, header: v.column.title,
        format: v.column.format ? <FormatData>{
          dateFormat: v.column.format.date_format,
          digitsInfo: v.column.format.digits_info
        } : null,
        cell: (element: any) => `${element[v.definition]}`
      })));
  }

}
