import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auto.guard';

const routes: Routes = [
  { path: 'login',  loadChildren: './modules/login/login.module#LoginModule' },
  { path: '**', redirectTo: 'portfolios' },
   {
     path: 'portfolios',
     loadChildren: './modules/portfolios/portfolios.module#PortfoliosModule', canActivate: [AuthGuard]
   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
