import { Component } from '@angular/core';
import { User } from './shared';
import { AuthenticationService } from './services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'storeTest';

  currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    logout($event: any) {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}
