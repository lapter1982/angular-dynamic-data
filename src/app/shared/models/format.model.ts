export interface FormatDate {
    dateFormat?: string;
}

export interface FormatNumber {
    digitsInfo?: string;
    period?: string;
}

export type FormatData = FormatDate & FormatNumber;
