import { FormatData } from './format.model';

export interface TableColumnDefinition {
    title: string;
    header: string;
    columnDef: string;
    cell: (element: any) => any;
    format?: Partial<FormatData>;
}
