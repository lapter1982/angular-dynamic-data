import { Pipe, PipeTransform } from '@angular/core';
import { FormatData } from '../models';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'customValue' })

export class CustomValuePipe implements PipeTransform {

    transform(val, format?: FormatData) {
        if (!format) {
            return val;
        }
            if (format.dateFormat) {
                return new DatePipe('en-US').transform(val, format.dateFormat);
            }
            const decimal = Number(format.digitsInfo.split('-').filter(item => item)[1]);
            return Number(val).toFixed(decimal);


        }
    }
